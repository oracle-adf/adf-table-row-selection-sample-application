package view.backing;

import java.util.Iterator;

import java.util.List;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDocument;
import oracle.adf.view.rich.component.rich.RichForm;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelBox;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.component.rich.output.RichMessages;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.model.RowKeySet;

public class SupportIncidents {
    private RichForm f1;
    private RichDocument d1;
    private RichPanelBox pbSupportIssues;
    private RichMessages m1;
    private RichTable tsupportIssues;
    private RichCommandButton cbSelectMany;
    private RichCommandButton cbSelectOne;

    public void setF1(RichForm f1) {
        this.f1 = f1;
    }

    public RichForm getF1() {
        return f1;
    }

    public void setD1(RichDocument d1) {
        this.d1 = d1;
    }

    public RichDocument getD1() {
        return d1;
    }

    public void setPbSupportIssues(RichPanelBox pb1) {
        this.pbSupportIssues = pb1;
    }

    public RichPanelBox getPbSupportIssues() {
        return pbSupportIssues;
    }

    public void setM1(RichMessages m1) {
        this.m1 = m1;
    }

    public RichMessages getM1() {
        return m1;
    }

    public void setTsupportIssues(RichTable t1) {
        this.tsupportIssues = t1;
    }

    public RichTable getTsupportIssues() {
        return tsupportIssues;
    }

    public void setCbSelectMany(RichCommandButton cb1) {
        this.cbSelectMany = cb1;
    }

    public RichCommandButton getCbSelectMany() {
        return cbSelectMany;
    }

    public void setCbSelectOne(RichCommandButton cb1) {
        this.cbSelectOne = cb1;
    }

    public RichCommandButton getCbSelectOne() {
        return cbSelectOne;
    }


    public String cbSelectMany_action() {
        // For learning purposes - show Select Many Button clicked
        System.out.println("Select Many Button has been Clicked");

        // RowKeySet Object can hold the selected rows from a user as follows
        RowKeySet rksSelectedRows =
            this.getTsupportIssues().getSelectedRowKeys();

        // Iterator object provides the ability to use hasNext(), next() and remove() against the selected rows
        Iterator itrSelectedRows = rksSelectedRows.iterator();

        // Get the data control that is bound to the table - e.g. OpenSupportItemsIterator
        DCBindingContainer bindings =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcIteratorBindings =
            bindings.findIteratorBinding("OpenSupportItemsIterator");

        // Information from binding that is specific to the rows
        RowSetIterator rsiSelectedRows =
            dcIteratorBindings.getRowSetIterator();

        // Loop through selected rows
        while (itrSelectedRows.hasNext()) {

            // Get key for selected row
            Key key = (Key)((List)itrSelectedRows.next()).get(0);

            // Use the key to get the data from the above binding that is related to the row
            Row myRow = rsiSelectedRows.getRow(key);

            // Display attribute of row in console output - would generally be bound to a UI component like a Label and or used to call another proces
            System.out.println(myRow.getAttribute("IssueID"));
        }
        
        return null;
    }

    public String cbSelectOne_action() {
        // For learning purposes - show Select One button clicked
        System.out.println("Select One Button has been Clicked");

        // Get bindings associated with the current scope, then access the one that we have assigned to our table - e.g. OpenSupportItemsIterator
        DCBindingContainer bindings =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcItteratorBindings =
            bindings.findIteratorBinding("OpenSupportItemsIterator");

        // Get an object representing the table and what may be selected within it
        ViewObject voTableData = dcItteratorBindings.getViewObject();

        // Get selected row
        Row rowSelected = voTableData.getCurrentRow();

        // Display attribute of row in console output - would generally be bound to a UI component like a Label and or used to call another proces
        System.out.println(rowSelected.getAttribute("IssueID"));

        return null;
    }


}
